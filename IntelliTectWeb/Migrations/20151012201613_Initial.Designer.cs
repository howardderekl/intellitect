using System;
using IntelliTectWeb.Data;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Relational.Migrations.Infrastructure;
using IntelliTectWeb.Models;

namespace IntelliTectWeb.Migrations
{
    [ContextType(typeof(IntelliTectContext))]
    partial class Initial
    {
        public override string Id
        {
            get { return "20151012201613_Initial"; }
        }
        
        public override string ProductVersion
        {
            get { return "7.0.0-beta5-13549"; }
        }
        
        public override void BuildTargetModel(ModelBuilder builder)
        {
            builder
                .Annotation("SqlServer:DefaultSequenceName", "DefaultSequence")
                .Annotation("SqlServer:Sequence:.DefaultSequence", "'DefaultSequence', '', '1', '10', '', '', 'Int64', 'False'")
                .Annotation("SqlServer:ValueGeneration", "Sequence");
            
            builder.Entity("IntelliTectWeb.Models.Person", b =>
                {
                    b.Property<int>("PersonId")
                        .GenerateValueOnAdd()
                        .StoreGeneratedPattern(StoreGeneratedPattern.Identity);
                    
                    b.Property<string>("EmailAddress");
                    
                    b.Property<string>("FirstName");
                    
                    b.Property<string>("LastName");
                    
                    b.Property<string>("MiddleInitial");
                    
                    b.Property<string>("PhoneNumber");
                    
                    b.Key("PersonId");
                });
        }
    }
}
