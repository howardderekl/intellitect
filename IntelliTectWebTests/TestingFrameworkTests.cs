﻿using Xunit;
using IntelliTectWeb.Controllers;
using Microsoft.AspNet.Mvc;

namespace IntelliTectWebTests
{
    public class TestingFrameworkTests
    {
        [Fact]
        public void PassingTest1()
        {
            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        public void PassingTest2()
        {
            Assert.Equal(5, Add(2, 3));
        }

        int Add(int x, int y)
        {
            return x + y;
        }

        [Fact]
        public void ControllerTest()
        {
            var controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
        }
    }
}