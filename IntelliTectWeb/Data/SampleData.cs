﻿using Microsoft.Data.Entity;
using Microsoft.Framework.DependencyInjection;
using System;
using System.Linq;
using IntelliTectWeb.Models;


namespace IntelliTectWeb.Data
{
    public static class SampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<IntelliTectContext>();
            if (context.Database.AsRelational().Exists())
            {
                if (!context.Persons.Any())
                {
                    var derek = context.Persons.Add(
                        new Person { LastName = "Howard", FirstName = "Derek", MiddleInitial = "L", PhoneNumber = "2084195327", EmailAddress = "howardderekl@gmail.com" }).Entity;
                    var tracie = context.Persons.Add(
                        new Person { LastName = "Howard", FirstName = "Tracie", PhoneNumber = "2084195382", EmailAddress = "howardtraciej@gmail.com" }).Entity;
                    var gaige = context.Persons.Add(
                        new Person { LastName = "Howard", FirstName = "Gaige" , MiddleInitial = "D", PhoneNumber = "2088218608", EmailAddress = "gaigehoward@gmail.com" }).Entity;

                    context.SaveChanges();
                }
            }
        }
    }
}
