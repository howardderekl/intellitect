﻿using System.Linq;
using System.Threading.Tasks;
using IntelliTectWeb.Data;
using IntelliTectWeb.Models;
using Microsoft.Data.Entity;

namespace IntelliTectWeb.Repos
{
    public class PersonRepo : IPersonRepo
    {
        private readonly IntelliTectContext _context;

        public PersonRepo(IntelliTectContext context)
        {
            _context = context;
        }

        public DbSet<Person> GetAllPersons()
        {
           return _context.Persons;
        }

        public async Task<Person> GetPersonByIdAsync(int personId)
        {
           return await _context.Persons.SingleOrDefaultAsync(b => b.PersonId == personId);
        }

        public async Task<int> InsertPersonAsync(Person person)
        {
            _context.Persons.Add(person);
            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdatePersonAsync(int personId, Person person)
        {
            person.PersonId = personId;
            _context.Persons.Attach(person);
            _context.Entry(person).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeletePersonByIdAsync(int personId)
        {
            var person = await GetPersonByIdAsync(personId);
            _context.Persons.Remove(person);
            return await _context.SaveChangesAsync();
        }
    }
}