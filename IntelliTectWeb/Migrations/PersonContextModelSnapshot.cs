using System;
using IntelliTectWeb.Data;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Relational.Migrations.Infrastructure;
using IntelliTectWeb.Models;

namespace IntelliTectWeb.Migrations
{
    [ContextType(typeof(IntelliTectContext))]
    partial class PersonContextModelSnapshot : ModelSnapshot
    {
        public override void BuildModel(ModelBuilder builder)
        {
            builder
                .Annotation("SqlServer:DefaultSequenceName", "DefaultSequence")
                .Annotation("SqlServer:Sequence:.DefaultSequence", "'DefaultSequence', '', '1', '10', '', '', 'Int64', 'False'")
                .Annotation("SqlServer:ValueGeneration", "Sequence");
            
            builder.Entity("IntelliTectWeb.Models.Person", b =>
                {
                    b.Property<int>("PersonId")
                        .GenerateValueOnAdd()
                        .StoreGeneratedPattern(StoreGeneratedPattern.Identity);
                    
                    b.Property<string>("EmailAddress");
                    
                    b.Property<string>("FirstName");
                    
                    b.Property<string>("LastName");
                    
                    b.Property<string>("MiddleInitial");
                    
                    b.Property<string>("PhoneNumber");
                    
                    b.Key("PersonId");
                });
        }
    }
}
