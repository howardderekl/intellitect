# README #

Sample project for IntelliTect interview.  Creating a People ASP.Net project.

## Version Information ##
* Version 1.0.0

## Author ##
* Derek Howard

## Setup Instructions ##
* Clone the repository - master branch
* Open the Solution file using Visual Studio 2015
* After the dependancies have been rebuilt, compile the project to detect the xUnit tests
* Using the Visual Studio Test Explorer, "Run All" tests
* Run the project to view the People CRUD operations via MVC 6 and EF 7-beta