﻿@ECHO OFF
Echo Checking that the Correct Verision of dnvm is installed
dnvm install 1.0.0-beta5
echo checking that the project is using the correct version of dnvm
dnvm use 1.0.0-beta5
ECHO Setup of the EF Database
dnx ef . migration add Initial
ECHO Apply the Model to the EF Database
dnx ef . migration apply