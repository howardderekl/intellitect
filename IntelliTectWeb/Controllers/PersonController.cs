﻿using IntelliTectWeb.Models;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Storage;
using Microsoft.Framework.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntelliTectWeb.Data;
using IntelliTectWeb.Repos;

namespace IntelliTectWeb.Controllers
{
    public class PersonController : Controller
    {
        [FromServices]
        public ILogger<PersonController> Logger { get; set; }

        private readonly IPersonRepo _personRepo;
     	public PersonController(IPersonRepo personRepo)
     	{
     		_personRepo = personRepo;
     	}

    public IActionResult Index()
        {
            var persons = _personRepo.GetAllPersons();
            return View(persons);
        }

        public async Task<ActionResult> Details(int id)
        {
            var person = await _personRepo.GetPersonByIdAsync(id);
            if (person == null)
            {
                Logger.LogInformation("Details: Item not found {0}", id);
                return HttpNotFound();
            }
            return View(person);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("FirstName", "LastName", "MiddleInitial", "PhoneNumber", "EmailAddress")] Person person)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _personRepo.InsertPersonAsync(person);
                    return RedirectToAction("Index");
                }
            }
            catch (DataStoreException)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes.");
            }
            return View(person);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var person = await _personRepo.GetPersonByIdAsync(id);
            if (person == null)
            {
                Logger.LogInformation("Edit: Person not found {0}", id);
                return HttpNotFound();
            }
                        
            return View(person);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(int id, [Bind("FirstName", "LastName", "MiddleInitial", "PhoneNumber", "EmailAddress")] Person person)
        {
            try
            {
                await _personRepo.UpdatePersonAsync(id, person);
                return RedirectToAction("Index");
            }
            catch (DataStoreException)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes.");
            }
            return View(person);
        }

        [HttpGet]
        [ActionName("Delete")]
        public async Task<ActionResult> ConfirmDelete(int id, bool? retry)
        {
            var person = await _personRepo.GetPersonByIdAsync(id);
            if (person == null)
            {
                Logger.LogInformation("Delete: Person not found {0}", id);
                return HttpNotFound();
            }
            ViewBag.Retry = retry ?? false;
            return View(person);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _personRepo.DeletePersonByIdAsync(id);
            }
            catch (DataStoreException)
            {
                return RedirectToAction("Delete", new { id = id, retry = true });
            }
            return RedirectToAction("Index");
        }
    }
}
