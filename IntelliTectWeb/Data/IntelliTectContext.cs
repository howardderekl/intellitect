﻿using IntelliTectWeb.Models;
using Microsoft.Data.Entity;

namespace IntelliTectWeb.Data
{
    public class IntelliTectContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
    }
}
