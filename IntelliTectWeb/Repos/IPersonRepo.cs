﻿using System.Threading.Tasks;
using IntelliTectWeb.Models;
using Microsoft.Data.Entity;

namespace IntelliTectWeb.Repos
{
    public interface IPersonRepo
    {
        DbSet<Person> GetAllPersons();
        Task<Person> GetPersonByIdAsync(int personId);
        Task<int> InsertPersonAsync(Person person);
        Task<int> UpdatePersonAsync(int personId, Person person);
        Task<int> DeletePersonByIdAsync(int personId);
    }
}